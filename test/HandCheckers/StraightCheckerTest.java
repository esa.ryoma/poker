/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HandCheckers;

import poker.Model.HandCreator;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import poker.Model.Card;
import poker.Model.Hand;
import poker.Model.Suit;

/**
 *
 * @author esa_r
 */
public class StraightCheckerTest {
    static Hand regular;
    static Hand ATo10;
    static Hand straight1To5;
    static Hand notStraight;
    static Hand fakeStraight13456;
    
    public StraightCheckerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("--------------");
        System.out.println("StraightChecker tests");
        
        HandCreator handCreator = new HandCreator();
        
        // Create all test hands
        regular = handCreator.createHandStraightRegular();
        System.out.println("Regular hand created: ");
        System.out.println(regular + "\n");
        
        ATo10 = handCreator.createHandStraightATo10();
        System.out.println("A-10 hand created: ");
        System.out.println(ATo10 + "\n");
        
        straight1To5 = handCreator.createHandStraight1To5();
        System.out.println("1-5 hand created: ");
        System.out.println(straight1To5 + "\n");
        
        notStraight = handCreator.createHandStraightNot();
        System.out.println("Not Straight hand created: ");
        System.out.println(notStraight + "\n");
        
        notStraight = handCreator.createHandFakeStraight13456();
        System.out.println("Fake Straight 1-3-4-5-6 hand created: ");
        System.out.println(fakeStraight13456 + "\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    
    
    /**
     * Test of containsWin method, of class StraightChecker.
     * Hand: Regular Straight
     */
    @Test
    public void testContainsWinRegular() {
        System.out.println("\n containsWin - Regular Straight:");
        StraightChecker instance = new StraightChecker();
        boolean result = instance.containsWin(regular);
        assertTrue("Should return true. Returned: " + result, result);
    }
    
    /**
     * Test of containsWin method, of class StraightChecker.
     * Hand: A-10 Straight
     */
    @Test
    public void testContainsWinATo10() {
        System.out.println("\n containsWin - A-10 Straight");
        StraightChecker instance = new StraightChecker();
        boolean result = instance.containsWin(ATo10);
        assertTrue("Should return true. Returned: " + result + "\n", result);
    }
    
    /**
     * Test of containsWin method, of class StraightChecker.
     * Hand: 1-5 Straight
     */
    @Test
    public void testContainsWin1To5() {
        System.out.println("\n containsWin - 1-5 Straight:");
        StraightChecker instance = new StraightChecker();
        boolean result = instance.containsWin(straight1To5);
        assertTrue("Should return true. Returned: " + result, result);
    }
    
    /**
     * Test of containsWin method, of class StraightChecker.
     * Hand: Not Straight
     */
    @Test
    public void testContainsWinNotStraight() {
        System.out.println("\n containsWin - Not Straight:");
        StraightChecker instance = new StraightChecker();
        boolean result = instance.containsWin(notStraight);
        assertFalse("Should return false. Returned: " + result, result);
    }
    
    /**
     * Test of containsWin method, of class StraightChecker.
     * Hand: Fake Straight 1-3-4-5-6
     */
    @Test
    public void testContainsWinFakeStraight13456() {
        System.out.println("\n containsWin - Fake Straight 1-3-4-5-6:");
        StraightChecker instance = new StraightChecker();
        boolean result = instance.containsWin(fakeStraight13456);
        assertFalse("Should return false. Returned: " + result, result);
    }
    
    /**
     * Test of getCombination method, of class StraightChecker.
     */
    @Test
    public void testGetCombination() {
        System.out.println("\n getCombination:");
        StraightChecker instance = new StraightChecker();
        Combinations expResult = Combinations.STRAIGHT;
        Combinations result = instance.getCombination();
        assertEquals("Did not return STRAIGHT, instead got: " + result, expResult, result);
    }
    
    
    // TESTS OF TEMPLATE METHOD checkHand
    // Testing if the combinations property of Hand class gets updated correctly
    // when receiving a winning hand.
    
    /**
     * Test of checkHand method, of class StraightChecker.
     * Hand: Regular Straight
     */
    @Test
    public void testCheckHandRegularStraight() {
        System.out.println("\n checkHand - Regular Straight:");
        StraightChecker instance = new StraightChecker();
        Hand resultHand = instance.checkHand(regular);
        boolean result = resultHand.getCombinations().contains(Combinations.STRAIGHT);
        assertTrue("Should return true. Returned: " + result, result);
    }
    
    /**
     * Test of checkHand method, of class StraightChecker.
     * Hand: A-10
     */
    @Test
    public void testCheckHandATo10() {
        System.out.println("\n checkHand - A-10:");
        StraightChecker instance = new StraightChecker();
        Hand resultHand = instance.checkHand(ATo10);
        boolean result = resultHand.getCombinations().contains(Combinations.STRAIGHT);
        assertTrue("Should return true. Returned: " + result, result);
    }
    
    /**
     * Test of checkHand method, of class StraightChecker.
     * Hand: 1-5
     */
    @Test
    public void testCheckHand1To5() {
        System.out.println("\n checkHand - 1-5:");
        StraightChecker instance = new StraightChecker();
        Hand resultHand = instance.checkHand(straight1To5);
        boolean result = resultHand.getCombinations().contains(Combinations.STRAIGHT);
        assertTrue("Should return true. Returned: " + result, result);
    }
    
    /**
     * Test of checkHand method, of class StraightChecker.
     * Hand: Not straight
     */
    @Test
    public void testCheckHandNotStraight() {
        System.out.println("\n checkHand - Not Straight:");
        StraightChecker instance = new StraightChecker();
        Hand resultHand = instance.checkHand(notStraight);
        boolean result = resultHand.getCombinations().contains(Combinations.STRAIGHT);
        assertFalse("Should return false. Returned: " + result, result);
    }
}
