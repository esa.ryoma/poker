
package HandCheckers;

import poker.Model.HandCreator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import poker.Model.Hand;

/**
 * Tests for FlushChecker class
 * @author Esa Ryömä
 */
public class FlushCheckerTest {
    static Hand diamonds;
    static Hand spades;
    static Hand hearts;
    static Hand clubs;
    static Hand notFlush;
    
    public FlushCheckerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("--------------");
        System.out.println("FlushChecker tests");
        HandCreator handCreator = new HandCreator();
        diamonds = handCreator.createHandFlushDiamonds();
        spades = handCreator.createHandFlushSpades();
        hearts = handCreator.createHandFlushHearts();
        notFlush = handCreator.createHandFlushNot();
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of containsWin method, of class FlushChecker.
     */
    @Test
    public void testContainsWin() {
        System.out.println("containsWin");
        Hand hand = null;
        FlushChecker instance = new FlushChecker();
        boolean expResult = false;
        boolean result = instance.containsWin(hand);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCombination method, of class FlushChecker.
     */
    @Test
    public void testGetCombination() {
        System.out.println("getCombination");
        FlushChecker instance = new FlushChecker();
        Combinations expResult = null;
        Combinations result = instance.getCombination();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
