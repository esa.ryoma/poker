
package HandCheckers;

import java.util.Comparator;
import poker.Model.Card;
import poker.Model.Suit;

/**
 * Class used as a Comparator for List.sort() methods. 
 * Cards can be organized by suits in hand.
 * @author Esa Ryömä
 */
public class SuitComparator implements Comparator<Card> {

    
    /**
     * Compares its two arguments for order. Returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
     */
    @Override
    public int compare(Card card1, Card card2) {
        if(card1.getSuit() == card2.getSuit()){ // suit is the same
            return 0;
        } else if (card1.getSuit() == Suit.HEARTS) { // Hearts "the lowest"
            return -1;
        } else if (card2.getSuit() == Suit.HEARTS) { // Hearts "the lowest"
            return 1;
        } else if (card1.getSuit() == Suit.DIAMONDS) { // Diamonds "the highest"
            return 1;
        } else if (card2.getSuit() == Suit.DIAMONDS) { // Diamonds "the highest"
            return -1;
        } else if (card1.getSuit() == Suit.CLUBS) { // Spades "2nd lowest"
            return -1;
        } else {
            return 1;
        }
        
    }

 
    
}
