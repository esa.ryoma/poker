
package HandCheckers;

/**
 * The HandChecker subclasses need to able to communicate back, which hands were 
 * found. 
 * @author Esa Ryömä
 */
public enum Combinations {
    TWOPAIRS,
    STRAIGHT,
    FLUSH,
    STRAIGHTFLUSH
}
