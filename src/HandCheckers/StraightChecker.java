
package HandCheckers;

import java.util.ArrayList;
import poker.Model.Card;
import poker.Model.Hand;

/**
 *
 * @author Esa Ryömä
 */
public class StraightChecker extends HandChecker {
    private HandChecker successor;
    
    public StraightChecker(HandChecker successor){
        super(successor);
    }
    
    public StraightChecker(){
        super();
    }
    
    
    // Todo: Suffers from LongMethod: use Extract Method to refactor.
    @Override
    public synchronized boolean containsWin(Hand hand) {
        // Make a copy of hand cards to avoid constant reference to hand.getCards()
        // Changes in copy --> changes in original as well
        ArrayList<Card> cards = new ArrayList();
        cards = (ArrayList<Card>)hand.getCards();
        
        //Of all combinations, StraightFlush is checked first, so we can see those results here.
        // If the hand contains straight flush, it also contains straight
        //System.out.println("Printing hand combinations in StraightChecker: " + hand.getCombinations());
        if (handHasStraightFlush(hand.getCombinations())){
            return true;
        }
        
        // Organize hand by card values
        cards.sort(new ValueComparator());
        
        if (handHasAce(cards)){
            //System.out.println("StraightChecker: Ace is in hand");
            
            if (handContains1to5(cards)){
                //System.out.println("StraightChecker: Straight 1-5");
                return true;
            } else if (handContainsAto10(cards)) {
                //System.out.println("StraightChecker: Straight A-10");
                return true;
            } else {
                //System.out.println("Ace is in hand, but NO STRAIGHT");
            }
        } else {
            //System.out.println("Ace NOT in hand");
            if (handContainsRegularStraight(cards)){ // Hand has any sequence of ascending numbers
                //System.out.println("StraightChecker: Straight any straight");
                return true;
            }
        }
        // No straight found
        return false;
        
    }
    
    @Override
    public Combinations getCombination() {
        return Combinations.STRAIGHT;
    }
    
    public boolean handHasAce(ArrayList<Card> cards){
        //System.out.println("First card is: " + cards.get(0).getValue());
        return cards.get(0).getValue() == 1;
        
    }
    
    public boolean handHasStraightFlush(ArrayList<Combinations> combinations){
        return combinations != null && combinations.contains(Combinations.STRAIGHTFLUSH);
    }
    
    public boolean handContainsAto10(ArrayList<Card> cards){
        for(int i = cards.size()-1; i >= 2; i--) {
            if (cards.get(i).getValue() - cards.get(i-1).getValue() != 1){
                return false; // quit loop, no straight A-10 in hand.
            }
        }
        return true;
    }
    
    public boolean handContains1to5(ArrayList<Card> cards){
        // If the first 2 cards are sequential
        if (cards.get(0).getValue() == cards.get(1).getValue()-1){
            // if the rest of the cards are sequential
            for(int i = 0; i <= cards.size()-2; i++) {
                if(cards.get(i).getValue() - cards.get(i+1).getValue() != -1){
                    return false; // quit loop, no straight in hand
                }
            }
            return true;
        } else { // first 2 cards were not sequential
            return false;
        }
        
        
    }
    
    public boolean handContainsRegularStraight(ArrayList<Card> cards){
        // Below loop ends when iterations exceed cards.size()-2 we only iterate up to 2nd to last card.
        for(int i = 0; i <= (cards.size()-2); i++) {
            if(cards.get(i).getValue() - cards.get(i+1).getValue() != -1){
                return false; // quit loop, no straight in hand
            }
        }
        return true;
    }
}
