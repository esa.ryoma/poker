
package HandCheckers;

import java.util.ArrayList;
import poker.Model.Hand;

/**
 *
 * @author Esa Ryömä
 */
public class StraightFlushChecker extends HandChecker {
    private StraightChecker straightChecker = new StraightChecker();
    private FlushChecker flushChecker = new FlushChecker();
    
    public StraightFlushChecker(HandChecker successor){
        super(successor);
    }
    
    public StraightFlushChecker(){
        super();
    }
    
    @Override
    public synchronized boolean containsWin(Hand hand) {
        // Hand contains both a straight and a flush
        if(straightChecker.containsWin(hand) && flushChecker.containsWin(hand)){
            return true;
        } else {
            return false;
        }
    }
    
     @Override
    public Combinations getCombination() {
        return Combinations.STRAIGHTFLUSH;
    }

    
}
