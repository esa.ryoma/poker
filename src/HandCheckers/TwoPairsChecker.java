
package HandCheckers;

import java.util.ArrayList;
import poker.Model.Card;
import poker.Model.Hand;

/**
 *
 * @author Esa Ryömä
 */
public class TwoPairsChecker extends HandChecker {
    
    public TwoPairsChecker(){
        super();
    }
    
    public TwoPairsChecker(HandChecker successor){
        super(successor);
    }
    
    @Override
    public synchronized boolean containsWin(Hand hand) {
        // Make a shallow clone of hand cards to avoid constant reference to hand.getCards()
        // Changes in copy --> changes in original as well
        ArrayList<Card> cards = new ArrayList();
        cards = (ArrayList<Card>)hand.getCards().clone();
        
        // Sort hand by value
        cards.sort(new ValueComparator());
        
        // Check if hand has "aa bb x" AND a != b
        if (cards.get(0).getValue() == cards.get(1).getValue() && 
                cards.get(2).getValue() == cards.get(3).getValue() &&
                cards.get(0).getValue() != cards.get(2).getValue()) {    
            return true;
            
        // Check if hand has "aa x bb" AND a != b    
        } else if (cards.get(0).getValue() == cards.get(1).getValue() && 
                cards.get(3).getValue() == cards.get(4).getValue() && 
                cards.get(0).getValue() != cards.get(3).getValue()) {
            return true;
            
        // Check if hand has "x aa bb" AND a != b
        } else if (cards.get(1).getValue() == cards.get(2).getValue() && 
                cards.get(3).getValue() == cards.get(4).getValue() &&
                cards.get(1).getValue() != cards.get(3).getValue()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Combinations getCombination() {
        return Combinations.TWOPAIRS;
    }
    
}
