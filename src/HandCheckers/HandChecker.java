package HandCheckers;

import java.util.ArrayList;
import java.util.LinkedList;
import poker.Model.Card;
import poker.Model.Hand;
/**
 * HandChecker is a decorator pattern abstract class. Every poker
 * hand is put through a series of HandChecker subclasses checking if the hand
 * contains a successful poker combination from most to least valuable.
 * HandCheckers add their enum value to an ArrayList of all combinations in 
 * a hand, if their combination appears.
 * @author Esa Ryömä
 */
public abstract class HandChecker {
    private ArrayList<Combinations> result;
    private HandChecker successor;
    protected ArrayList cards;
    
    public HandChecker(HandChecker successor){
        this.successor = successor;
        this.result = new ArrayList();
    }
    
    public HandChecker(){
        
    }
    
    /**
     * checkHand is a Template Method: it uses abstract methods, which
     * are specified in subclasses (FlushChecker etc.).
     * 
     * The method is given a hand of cards, which are given down the line of
     * decorators as parameters. The same Hand object gets modified in each
     * decorator: if a combination appears in the hand, the Combination is added
     * into hand.combinations.
     * @param cards Hand of cards to inspect
     * @return The modified Hand object
     */
    public synchronized Hand checkHand(Hand hand){
        if (successor != null){
            successor.checkHand(hand);
            checkAndAddCombination(hand);
            return hand;
        } else {
            checkAndAddCombination(hand);
            return hand;
        }
    }
    
    public void checkAndAddCombination(Hand hand){
        if(containsWin(hand)) {
            hand.addCombination(getCombination());
        }
    }
    
    /**
     * Method specified in subclasses. Checks if the combination in question exists in the cards given as parameters.
     * @param cards ArrayList of cards to check
     * @return True, if combination exists in cards. False, if not.
     */
    public abstract boolean containsWin(Hand hand);
    
    /**
     * @return The combination which this Checker checks against.
     */
    public abstract Combinations getCombination();
    
    
}
