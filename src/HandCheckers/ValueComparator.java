
package HandCheckers;

import java.util.Comparator;
import poker.Model.Card;

/**
 * Class used as a Comparator for List.sort() methods. 
 * Cards can be organized by their values
 * @author Esa Ryömä
 */
public class ValueComparator implements Comparator<Card> {

    /**
     * Compares its two arguments for order. Returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
     */
    @Override
    public int compare(Card card1, Card card2) {
        return card1.getValue() - card2.getValue();
    }
    
}
