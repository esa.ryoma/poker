
package HandCheckers;

import java.util.ArrayList;
import java.util.LinkedList;
import poker.Model.Card;
import poker.Model.Hand;

/**
 *
 * @author Esa Ryömä
 */
public class FlushChecker extends HandChecker {
    private HandChecker successor;
    private ArrayList result;
    
    public FlushChecker(HandChecker successor){
        super(successor);
    }
    
    public FlushChecker(){
        super();
    }
    
    
    @Override
    public synchronized boolean containsWin(Hand hand){
        /* 
        * In HandChecker checkHand() template method the result variable
        * contain all combinations which have been verified until that moment.
        * StraightFlush is checkd first, so we can see those results here.
        */
        
        // If the hand contains straight flush, it also contains flush
        if (hand.getCombinations() != null && hand.getCombinations().contains(Combinations.STRAIGHTFLUSH)){
            return true;
        }
        
       
        

        // Organize hand by suit
        hand.getCards().sort(new SuitComparator());
        
        // If first and last card are the same suit, the hand contains a flush
        return hand.getCards().get(0).getSuit() == hand.getCards().get(hand.getCards().size()-1).getSuit();
        
    }
    
    @Override
    public Combinations getCombination(){
        return Combinations.FLUSH;
    }
    
}
