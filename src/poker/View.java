
package poker;
import poker.Model.Hand;
import java.util.ArrayList;
import java.util.Observer;
import java.util.Observable;
import poker.Model.Player;
import HandCheckers.Combinations;


/**
 * View class of Model-View-Controller. 
 * Class Observes Controller: when Controller signales a change in a Players status,
 * View receives a Player object which it can use to check data.
 * 
 * This is encapsulation breaking for MVC model, as now View must be aware what is a Player object.
 * Todo: Refactor to better encapsulate - Player object could be sent as a memento which Controller can decode but View not
 * @author Esa Ryömä
 */
public class View implements Observer {
    Controller controller = null;
    
    public void printHand(String hand){
        System.out.println(hand);
    }
    
    public void printName(String name){
        System.out.println(name + ":");
    }
    
    public void printResults(String results){
        if(results.equals("")) {
            System.out.println("No win...");
        } else {
            System.out.println("WIN!");
            System.out.println("Hand contains: " + results);
        }
    }
    
    public synchronized void update(Observable observable, Object object){
        Player player;
        try {
            player = (Player)object;
            System.out.println("Player: " + player.getName());
            printHand(player.getCards().toString());
            
            String playersCombinations = "";
            for (Combinations combination : player.getCombinations()){
                if (combination != null){
                    if (isLastCombination(combination, player.getCombinations())){
                        playersCombinations += combination;
                    } else {
                        playersCombinations += combination + ", ";
                    }
                        
                }
                
            }
            
            printResults(playersCombinations);
            System.out.println("------------");
            
        } catch (ClassCastException error){
            System.out.println("Error when casting Observable into Player.");
            System.out.println(error.getStackTrace());
        } catch (Error error){
            System.out.println("Error when printing player data.");
            System.out.println(error.getStackTrace());
        }
        
        
    }
    
    public void setController(Controller controller){
        this.controller = controller;
    }
    
    public boolean isLastCombination(Combinations combination, ArrayList<Combinations> combinations){
        return combinations.indexOf(combination) == combinations.size()-1;
    }
}
