
package poker;

import poker.Model.*;
import HandCheckers.*;
import java.util.ArrayList;

/**
 * Poker Hand randomizer and checker<br>
 * 
 * PROGRAM COMPOSITION<br>
 * <ul>
 *      <li>MVC - pattern:
 *          <ul>
            <li>Model for operations and datastorage. No </li>
 *          <li>View for printing out results</li>
 *          <li>Controller for communication between M & C</li>
 *          </ul>
 *      </li>
 *      <li>Design Patterns:
 *          <ul>
 *          <li>Observer-Observable between Player-Controller-View. Still in a problematic state as it skips Model from MVC.</li>
 *          <li>Decorator - HandChecker class decorates Hand objects with results</li>
 *          <li>Template Method in HandChecker abstract class</li>
 *          </ul>
 *      <li>Other notes/techniques:
 *          <ul>
 *          <li>Some tests created for HandCheckers but no time for more.</li>
 *          <li>ExtractMethod/ExtractClass used extensively to ease readability and negate need for comments.</li>
 *          <li>Players are individual threads which update Observers when done with drawing cards and checking results.</li>
 *          </ul>
 *      </li>
 * </ul>
 * @author Esa Ryömä
 */
public class Main {

    public static void main(String[] args) {
        /*--------------------
        * SET UP
        */
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);
        view.setController(controller);
        model.setController(controller);
        
        controller.addObserver(view);
        
        
        // TwoPairsChecker is given as the first decorator
        /*
        Player player1 = new Player("Mikko", deck, discard, twoPairsChecker);
        Player player2 = new Player("Liisa", deck, discard, twoPairsChecker);
        
        Thread playerThread1 = new Thread(player1);
        Thread playerThread2 = new Thread(player2);
        */
        
        // Array of players
        String players[] = {
            "Pertti",
            "Mikko",
            "Liisa",
            "Hermanni"
        };
        
        for (String playerName : players){
            controller.addPlayer(playerName);
        }
        
        //player1.addObserver(controller);
        //player2.addObserver(controller);
       
        
        // Convert players to threads
        /*
        ArrayList<Thread> playerThreads = new ArrayList();
        for (Player player : players){
            playerThreads.add(new Thread(player));
        }
        */
        
        /*
        * SET UP ENDS
        -----------------*/
        
        /*-------------------- 
        * HARDCODED PART OF PROGRAM - here only for easy display of results
        * HANDS AND CHECKS PRINTED FOR CODE REVIEWER
        */
        
        StraightFlushChecker straightFlushChecker = new StraightFlushChecker();
        FlushChecker flushChecker = new FlushChecker(straightFlushChecker);
        StraightChecker straightChecker = new StraightChecker(flushChecker);
        TwoPairsChecker twoPairsChecker = new TwoPairsChecker(straightChecker);
        HandCreator handCreator = new HandCreator();
        Hand resultHand;
        
        System.out.println("---------------");
        System.out.println("HARDCODED EXAMPLES");
        // Straight
        Hand exampleStraight1 = handCreator.createHandStraight1To5();
        System.out.println("Straight 1-5: ");
        System.out.println(exampleStraight1);
        resultHand = twoPairsChecker.checkHand(exampleStraight1);
        System.out.println("Should have Straight: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");
        
        // NOT Straight
        Hand exampleStraight2 = handCreator.createHandStraightNot();
        System.out.println("Not Straight: ");
        System.out.println(exampleStraight2);
        resultHand = twoPairsChecker.checkHand(exampleStraight2);
        System.out.println("Should NOT have Straight: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");
        
        // Flush
        Hand exampleFlush1 = handCreator.createHandFlushDiamonds();
        System.out.println("Flush of diamonds: ");
        System.out.println(exampleFlush1);
        resultHand = twoPairsChecker.checkHand(exampleFlush1);
        System.out.println("Should have Flush: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");
        
        // NOT Flush
        Hand exampleFlush2 = handCreator.createHandFlushNot();
        System.out.println("NOT flush hand: ");
        System.out.println(exampleFlush2);
        resultHand = twoPairsChecker.checkHand(exampleFlush2);
        System.out.println("Should NOT have flush: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");
        
        // Two Pairs
        Hand exampleTwoPairs1 = handCreator.createTwoPairsXXYZY();
        System.out.println("Two Pairs hand: ");
        System.out.println(exampleTwoPairs1);
        resultHand = twoPairsChecker.checkHand(exampleTwoPairs1);
        System.out.println("Should have Two Pairs: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");
        
        // NOT Two Pairs
        Hand exampleTwoPairs2 = handCreator.createTwoPairsNot();
        System.out.println("NO Two Pairs hand: ");
        System.out.println(exampleTwoPairs2);
        resultHand = twoPairsChecker.checkHand(exampleTwoPairs2);
        System.out.println("Should NOT have Two Pairs: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");
        
        // Straight Flush
        Hand exampleStraightFlush1 = handCreator.createHandStraightFlush1To5();
        System.out.println("Straight Flush hand: ");
        System.out.println(exampleStraightFlush1);
        resultHand = twoPairsChecker.checkHand(exampleStraightFlush1);
        System.out.println("Should have Straight Flush: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");
        
        // NOT Straight Flush
        Hand exampleStraightFlush2 = handCreator.createHandStraightFlushNotFlush();
        System.out.println("NOT Straight Flush hand: ");
        System.out.println(exampleStraightFlush2);
        resultHand = twoPairsChecker.checkHand(exampleStraightFlush2);
        System.out.println("Should NOT have Straight Flush: ");
        System.out.println(resultHand.getCombinations());
        System.out.println("");

        
        /*
        * HARD CODED SEGMENT ENDS
        ----------*/
        System.out.println("HARDCODED EXAMPLES END");
        System.out.println("\n--------------\n");                
                
        /*---------------
        * ENCAPSULATED, MULTITHREADED, DESIGN PATTERN VERSION OF PROGRAM
        * Initiate random hands with threaded players
        */
        System.out.println("--------------");
        System.out.println("INITIATE PLAYER THREADS WITH RANDOM HANDS");
        controller.startAllPlayers();
        /*
        * END
        -----------------*/
        
    }
    
}
