
package poker;

import HandCheckers.Combinations;
import java.util.ArrayList;
import poker.Model.Model;
import poker.Model.Player;
import java.util.Observer;
import java.util.Observable;
import poker.Model.Card;
import poker.Model.Hand;

/**
 * Controller class between View and Model
 * Class implements Observer, observing the Players and printing their results.
 * @author Esa Ryömä
 */
public class Controller extends Observable implements Observer {
    private Model model;
    private View view;
    
    public Controller(Model model, View view){
        this.model = model;
        this.view = view;
        addObserver(view);
    }
    
    /**
     * Model pushes updated Player information to the Controller,
     * which proceeds to update View.
     */
    public synchronized void update(Observable observable, Object object){
        Player player;
        try {
            // Cast the Observable player object into Player class before sending it to view for handling
            player = (Player)observable;
            //System.out.println(player.getCards().toString());
            //System.out.println(player.getCombinations());
            setChanged();
            notifyObservers(player);
            
        } catch (Error error){
            System.out.println("Error when casting Observable into Player.");
            System.out.println(error.getMessage());
        }
        
        this.notifyAll();
        
    }

    public String getCards(Player player){
        return player.getHand().getCards().toString();
    }
    
    public String getCombinations(Player player){
        return player.getHand().getCombinations().toString();
    }
    
    public String getName(Player player){
        return player.getName();
    }
    
    @Override
    public synchronized void addObserver(Observer o) {
        super.addObserver(o); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList getPlayers(){
        return model.getPlayers();
    }
    
    public void startAllPlayers(){
        model.startAllPlayers();
    }
    
    public void addPlayer(String player){
        model.addPlayer(player);
    }
    
}
