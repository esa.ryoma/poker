
package poker.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author Esa Ryömä
 */
public class Deck {
    /* List chosen as the deck data structure:
            Compared to arrays, List has a bit more memory overhead,
            but the List interface functionality gives easier opportunities for 
            modifying and adding new features to the program.
    
        ArrayList vs. LinkedList:
            According to https://dzone.com/articles/arraylist-vs-linkedlist-vs
            LinkedList is more efficient on add/remove operation but slower on 
            get/set. Operations used in this program are mostly add/remove
            (drawing a card is removing from deck and adding to the hand),
            LinkedList it is!
    */
    LinkedList<Card> cards = new LinkedList();
    
    public Deck() {
        // A deck of cards: different suits in Enum Suit, numbers 1-13 each suit
        for (Suit suit : Suit.values()){
            for (int i = 1; i <= 13; i++){
                cards.add(new Card(i, suit));
            }
        }
    } 
    
    public Deck(boolean empty){
    }
    
    
    
    public synchronized LinkedList<Card> getCards(){
        return cards;
    }
    
    
    @Override
    public synchronized String toString(){
        String allCards = "";
        for (Card card : cards){
            allCards += card.toString() + "\n";
        }
        return allCards;
            
    }
    
    public synchronized void shuffle(){
        for (int i = 0; i <= 3; i++){
            Collections.shuffle(cards);
        }
        notifyAll();
    }
    
    public synchronized void reshuffle(Deck discard){
        this.cards.addAll(discard.getCards());
        discard.clear();
    }
    
    public synchronized Card drawTopCard(){
        return cards.pop();
    }
    
    public synchronized int size(){
        return cards.size();
    }
    
    public synchronized void addCards(ArrayList<Card> hand){
        cards.addAll(hand);
        notifyAll();
    }
    
    public synchronized void clear(){
        cards.clear();
        notifyAll();
    }
    
}
