package poker.Model;

import java.util.ArrayList;
import poker.Model.Card;
import poker.Model.Hand;
import poker.Model.Suit;

/**
 * Common class for all HandCheckerTests. 
 * Contains methods for creating all the different test hands.
 * 
 * Todo: Suffers from Huge Class - difficult to read
 * Refactoring: Could also be created as abstract class / interface, with each type of and being a subclass
 * @author Esa Ryömä
 */
public class HandCreator {
    
    // STRAiGHT
    /**
     * Form a straight A to 10
     * @return Hand containing A-K-Q-J-10 of Clubs
     */
    public Hand createHandStraightATo10(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.DIAMONDS));
        for (int i = 13; i >= 10; i--){
            tempList.add(new Card(i, Suit.CLUBS));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a straight 1 to 5
     * @return Hand containing 1-2-3-4-5 of Clubs
     */
    public Hand createHandStraight1To5(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.DIAMONDS));
        for (int i = 2; i <= 5; i++){
            tempList.add(new Card(i,Suit.CLUBS));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a regular straight (no Ace)
     * @return Hand containing 3-4-5-6-7 of Clubs
     */
    public Hand createHandStraightRegular(){
        ArrayList<Card> tempList = new ArrayList();
        for (int i = 1; i <= 5; i++){
            tempList.add(new Card(i+2, Suit.CLUBS));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a definitely NOT straight hand
     * @return Hand containing 1-3-6-8-13 of Clubs
     */
    public Hand createHandStraightNot(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(3, Suit.CLUBS));
        tempList.add(new Card(6, Suit.CLUBS));
        tempList.add(new Card(8, Suit.CLUBS));
        tempList.add(new Card(13, Suit.CLUBS));
        return new Hand(tempList);
    }
    
    /**
     * Form an edge case, problematic fake straight
     * @return Hand containing 1-3-4-5-6 of Clubs
     */
    public Hand createHandFakeStraight13456(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(3, Suit.CLUBS));
        tempList.add(new Card(4, Suit.CLUBS));
        tempList.add(new Card(5, Suit.CLUBS));
        tempList.add(new Card(6, Suit.CLUBS));
        return new Hand(tempList);
    }
    
    // FLUSH
    /**
     * Form a flush of clubs
     * @return Hand containing 1-3-4-5-6 of Clubs
     */
    public Hand createHandFlushClubs(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(3, Suit.CLUBS));
        tempList.add(new Card(6, Suit.CLUBS));
        tempList.add(new Card(8, Suit.CLUBS));
        tempList.add(new Card(13, Suit.CLUBS));
        return new Hand(tempList);
    }
    
    /**
     * Form a flush of diamonds
     * @return Hand containing 1-3-4-5-6 of diamonds
     */
    public Hand createHandFlushDiamonds(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.DIAMONDS));
        tempList.add(new Card(3, Suit.DIAMONDS));
        tempList.add(new Card(6, Suit.DIAMONDS));
        tempList.add(new Card(8, Suit.DIAMONDS));
        tempList.add(new Card(13, Suit.DIAMONDS));
        return new Hand(tempList);
    }
    
    /**
     * Form a flush of spades
     * @return Hand containing 3-4-5-6-7 of spades
     */
    public Hand createHandFlushSpades(){
        ArrayList<Card> tempList = new ArrayList();
        for (int i = 1; i <= 5; i++){
            tempList.add(new Card(i+2, Suit.SPADES));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a flush of hearts
     * @return Hand containing 3-4-5-6-7 of hearts
     */
    public Hand createHandFlushHearts(){
        ArrayList<Card> tempList = new ArrayList();
        for (int i = 1; i <= 5; i++){
            tempList.add(new Card(i+2, Suit.HEARTS));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a definitely NOT flush hand
     * @return A hand containing 1-Clubs, 4-Diamonds, 6-Clubs,
     * 10-Spades, 2-Clubs
     */
    public Hand createHandFlushNot(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1,Suit.CLUBS));
        tempList.add(new Card(4,Suit.DIAMONDS));
        tempList.add(new Card(6,Suit.CLUBS));
        tempList.add(new Card(10,Suit.SPADES));
        tempList.add(new Card(2,Suit.CLUBS));
        return new Hand(tempList);
    }
    
    // TWO PAIRS
    public Hand createTwoPairsXYXYZ(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(3, Suit.CLUBS));
        tempList.add(new Card(1, Suit.DIAMONDS));
        tempList.add(new Card(3, Suit.HEARTS));
        tempList.add(new Card(6, Suit.CLUBS));
        return new Hand(tempList);
    }
    
    public Hand createTwoPairsXXYZY(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(1, Suit.DIAMONDS));
        tempList.add(new Card(6, Suit.DIAMONDS));
        tempList.add(new Card(3, Suit.HEARTS));
        tempList.add(new Card(6, Suit.SPADES));
        return new Hand(tempList);
    }
    
    public Hand createTwoPairsNot(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(1, Suit.DIAMONDS));
        tempList.add(new Card(6, Suit.DIAMONDS));
        tempList.add(new Card(3, Suit.HEARTS));
        tempList.add(new Card(5, Suit.SPADES));
        return new Hand(tempList);
    }
    
    // STRAIGHT FLUSH
    /**
     * Form a straight flush A to 10
     * @return Hand containing A-K-Q-J-10 of Clubs
     */
    public Hand createHandStraightFlushATo10(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        for (int i = 13; i >= 10; i--){
            tempList.add(new Card(i, Suit.CLUBS));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a straight flush 1 to 5
     * @return Hand containing 1-2-3-4-5 of Clubs
     */
    public Hand createHandStraightFlush1To5(){
        ArrayList<Card> tempList = new ArrayList();
        for (int i = 1; i <= 5; i++){
            tempList.add(new Card(i,Suit.CLUBS));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a regular straight flush (no Ace)
     * @return Hand containing 3-4-5-6-7 of Clubs
     */
    public Hand createHandStraightFlushRegular(){
        ArrayList<Card> tempList = new ArrayList();
        for (int i = 1; i <= 5; i++){
            tempList.add(new Card(i+2, Suit.CLUBS));
        }
        return new Hand(tempList);
    }
    
    /**
     * Form a definitely NOT straight flush hand: <br>
     * values: not straight<br> 
     * suits: All the same
     * @return Hand containing 1-3-6-8-13 of Clubs
     */
    public Hand createHandStraightFlushNotStraight(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(3, Suit.CLUBS));
        tempList.add(new Card(6, Suit.CLUBS));
        tempList.add(new Card(8, Suit.CLUBS));
        tempList.add(new Card(13, Suit.CLUBS));
        return new Hand(tempList);
    }
    
    /**
     * Form a definitely NOT straight flush hand: <br>
     * values: straight<br> 
     * suits: not the same
     * @return Hand containing 3-4-5-6-7 of mostly same suit but not all
     */
    public Hand createHandStraightFlushNotFlush(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(3, Suit.HEARTS));
        tempList.add(new Card(4, Suit.DIAMONDS));
        tempList.add(new Card(5, Suit.HEARTS));
        tempList.add(new Card(6, Suit.HEARTS));
        tempList.add(new Card(7, Suit.HEARTS));
        return new Hand(tempList);
    }
    
    /**
     * Form an edge case, problematic fake straight flush
     * @return Hand containing 1-3-4-5-6 of Clubs
     */
    public Hand createHandFakeStraightFlush13456(){
        ArrayList<Card> tempList = new ArrayList();
        tempList.add(new Card(1, Suit.CLUBS));
        tempList.add(new Card(3, Suit.CLUBS));
        tempList.add(new Card(4, Suit.CLUBS));
        tempList.add(new Card(5, Suit.CLUBS));
        tempList.add(new Card(6, Suit.CLUBS));
        return new Hand(tempList);
    }
    
}
