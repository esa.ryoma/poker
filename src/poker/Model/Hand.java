
package poker.Model;

import HandCheckers.Combinations;
import java.util.ArrayList;

/**
 *
 * @author Esa Ryömä
 */
public class Hand {
    private ArrayList<Card> cards = new ArrayList();
    private ArrayList<Combinations> combinations = new ArrayList();

    public Hand(ArrayList<Card> cards){
        for (Card card : cards){
            this.cards.add(card);
        }
        
    }
    
    public Hand(){
        this.cards = new ArrayList();
    }
    
    public ArrayList<Card> getCards() {
        return cards;
    }

    public ArrayList<Combinations> getCombinations() {
        return combinations;
    }

    public void addCombination(Combinations combination) {
        this.combinations.add(combination);
    }
    
    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }
    
    public void addCard(Card card){
        this.cards.add(card);
    }
    
    /**
     * Empties hand of cards and its combinations
     */
    public void clear(){
        this.cards.clear();
        this.combinations.clear();
    }
    
    @Override
    public String toString(){
        return cards.toString();
    }
    
}
