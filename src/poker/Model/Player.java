
package poker.Model;

import HandCheckers.HandChecker;
import HandCheckers.Combinations;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Players are threads which run individually.
 * They are also observable as per the Observer design pattern. Once a player
 * has received their hand, checked their result, it notifies the Observer,
 * which then pushes the information to the rest of the progaram.<br><br>
 * Players have a common deck and discard pile. When deck empties, discard pile
 * is shuffled to the now empty deck.
 * @author Esa Ryömä
 */
public class Player extends Observable implements Runnable {
    private String name;
    private boolean stopped;
    private Deck deck;
    private HandChecker firstCheckerInChain;
    private Deck discard;
    private Hand hand = new Hand();
    private final int howManyHands = 50;
    
    /**
     * 
     * @param name Individual name for this player
     * @param deck Deck object common to all players is given to the player, 
     * so that the player can draw cards individually, without Model working as
     * a MiddleMan. Drawing cards from Deck object is synchronized so no
     * simultaneous draws should occur.
     */
    public Player(String name, Deck deck, Deck discard, HandChecker firstCheckerInChain){
        this.name = name;
        stopped = false;
        this.deck = deck;
        this.firstCheckerInChain = firstCheckerInChain;
        this.discard = discard;
    }

    @Override
    // Hard coded limit set
    public void run() {
        int k = 0;
        while (!stopped && k < howManyHands){
            
            
            // Draw 5 cards
            for (int i = 0; i <= 4; i++){
                if (deck.size() == 0){ // player drew the last card
                    deck.reshuffle(discard); // add the common discard to the deck and shuffle
                }
                hand.addCard(deck.drawTopCard());
            }
            
            // check for win
            firstCheckerInChain.checkHand(hand);
            
            setChanged();
            
            // notifyObservers(Object arg)
            // Each observer has its update method called with two arguments: this observable object and the arg argument.
            // hasChanged() is called automatically
            notifyObservers();
            
            /*
            Below code to be used if only winning hands need to be updated forward
            
            // If hand has been proven to contain combinations, 
            // update the players Observers that a change has occurred.
            
            if(hand.getCombinations() != null){
                setChanged();
                
                // notifyObservers(Object arg)
                // Each observer has its update method called with two arguments: this observable object and the arg argument.
                // hasChanged() is called automatically
                notifyObservers();
            }
            */
            
            // discard hand into discard pile
            discard.addCards(hand.getCards());
            hand.clear();
            //System.out.println("Hand should be empty, hand size: " + hand.getCards().size());
            //System.out.println("Discard should have added cards: " + discard.size());
            
            k++;
        }
        
    }
    
    public ArrayList<Card> getCards(){
        return hand.getCards();
    }
    
    public ArrayList<Combinations> getCombinations(){
        return hand.getCombinations();
    }
    
    public Hand getHand(){
        return hand;
    }
    
    public String getName(){
        return name;
    }
    
    public void stop(){
        stopped = true;
    }
    
    @Override
    public synchronized void addObserver(Observer o) {
        super.addObserver(o); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
