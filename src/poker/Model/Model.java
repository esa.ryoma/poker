
package poker.Model;

import HandCheckers.FlushChecker;
import HandCheckers.HandChecker;
import HandCheckers.StraightChecker;
import HandCheckers.StraightFlushChecker;
import HandCheckers.TwoPairsChecker;
import java.util.ArrayList;
import poker.Controller;

/**
 *
 * @author Esa Ryömä
 */
public class Model {
    private ArrayList<Thread> players = new ArrayList();
    private Deck deck = new Deck();
    private Deck discard = new Deck(true);
    private Controller controller;
    private StraightFlushChecker straightFlushChecker = new StraightFlushChecker();
    private FlushChecker flushChecker = new FlushChecker(straightFlushChecker);
    private StraightChecker straightChecker = new StraightChecker(flushChecker);
    private TwoPairsChecker twoPairsChecker = new TwoPairsChecker(straightChecker);
    
    
    // First checker in line of decorators - given to new created players
    private final HandChecker firstChecker = twoPairsChecker;
    
    public Model(){
        deck.shuffle();
        
    }
    
    public void setController(Controller controller){
        this.controller = controller;
    }
    
    public ArrayList getPlayers(){
        return players;
    }
    
    public void startAllPlayers(){
        for(Thread player : players){
            player.start();
        }
    }
    
    public void addPlayer(String playerName){
        Player newPlayer = new Player(playerName, deck, discard, firstChecker);
        newPlayer.addObserver(controller);
        players.add(new Thread(newPlayer));
    }
}
