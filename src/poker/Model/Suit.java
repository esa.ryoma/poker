
package poker.Model;

public enum Suit {
    HEARTS,
    CLUBS,
    SPADES,
    DIAMONDS
}
